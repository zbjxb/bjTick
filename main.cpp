#if defined(UNICODE) && !defined(_UNICODE)
    #define _UNICODE
#elif defined(_UNICODE) && !defined(UNICODE)
    #define UNICODE
#endif

/**
 * enable ListView_Get/SetView macros or messages.
 */
#pragma comment(linker,"\"/manifestdependency:type='win32' \
name='Microsoft.Windows.Common-Controls' version='6.0.0.0' \
processorArchitecture='*' publicKeyToken='6595b64144ccf1df' language='*'\"")

#include <tchar.h>
#include <windows.h>
#include <commctrl.h>
#include <iostream>
#include <functional>
#include <fstream>

#include "global/globals.h"
#include "msg/msg_processor.h"
#include "utils/RAII.h"
#include "wnds/ShellFolderInView.h"
#include "cmds/cmds.h"

/*  Declare Windows procedure  */
LRESULT CALLBACK WindowProcedure (HWND, UINT, WPARAM, LPARAM);

/*  Make the class name into a global variable  */
TCHAR szClassName[ ] = _T("CodeBlocksWindowsApp");


/////////////////////////////////////////////////////////////
/////////// GLogger setting ///////////////////////
bool setupLogger (tcstr& filePath, tostream** fileStream)
{
	std::wofstream* logFile = new std::wofstream ();
	logFile->exceptions (std::wofstream::failbit | std::wofstream::badbit);
	ExitCaller (logFile, ([=]() { delete logFile; }));

	try
	{
		logFile->open (filePath, std::ios::trunc);
		logFile->imbue (std::locale ("CHS"));
		tcout.imbue (std::locale ("CHS"));
		setGLogger (*logFile);
		*fileStream = logFile;
		DelExitCaller (logFile);
		return true;
	}
	catch (const std::wofstream::failure& e)
	{
		TGuardValue<bool> guard (GStdOutputIsOpen, true);
		GLog << _T ("ERROR::WinMain::FILE_NOT_SUCCESSFULLY_OPEN") << std::endl;
		return false;
	}
}

/////////////////////////////////////////////////////////////
/////////////////

int WINAPI WinMain (HINSTANCE hThisInstance,
                     HINSTANCE hPrevInstance,
                     LPSTR lpszArgument,
                     int nCmdShow)
{
#pragma region "Specify a log file to record events."
	tostream* logStream;
	if (!setupLogger (_T ("debug.log"), &logStream))
	{
		return -1;
	}
	ExitCaller (logStream, ([=]() {delete logStream; }));
#pragma endregion

	/// If you want to display logs in standard output, must call this function.
	/// Default is close.
	openStdOutput ();

	init_cmdnames_map ();
	{
		FCmdBasePtr copy_cmd0 (new FCopyToCmd);
		FCmdBasePtr copy_cmd1 (new FCopyToCmd);
		FCmdBasePtr move_cmd0 (new FMoveCmd);
		FCmdBasePtr move_cmd1 (new FMoveCmd);

		FComposeCmdPtr cmd_obj0(new FComposeCmd);
		cmd_obj0->addCmd (copy_cmd0);
		cmd_obj0->addCmd (copy_cmd1);
		cmd_obj0->addCmd (move_cmd0);
		cmd_obj0->addCmd (move_cmd1);

		FComposeCmdPtr cmd_obj1 (new FComposeCmd);
		GLog << ">>>>>>>>>>>>>>>>>>>\n";
		cmd_obj0->dump ();
		GLog << "=====================\n";
		bool ret = cmd_obj1->addCmd (cmd_obj0);
		cmd_obj1->dump ();
		GLog << "<<<<<<<<<<<<<<<<<<<\n";

		std::vector<tstr> paths_src = {
			_T ("D:/test/a"),
			_T ("D:/test/a/d.txt"),
			_T ("D:/test/b.txt"),
		};
		copy_cmd0->m_from.add (paths_src.begin (), paths_src.end ());
		copy_cmd0->m_to.add (_T ("D:/test/c"));
		//copy_cmd0->run ();
		cmd_obj1->run ();
	}

    MSG messages;            /* Here messages to the application are saved */
    WNDCLASSEX wincl;        /* Data structure for the windowclass */

    /* The Window structure */
    wincl.hInstance = hThisInstance;
    wincl.lpszClassName = szClassName;
    wincl.lpfnWndProc = WindowProcedure;      /* This function is called by windows */
    wincl.style = CS_DBLCLKS;                 /* Catch double-clicks */
    wincl.cbSize = sizeof (WNDCLASSEX);

    /* Use default icon and mouse-pointer */
    wincl.hIcon = LoadIcon (NULL, IDI_APPLICATION);
    wincl.hIconSm = LoadIcon (NULL, IDI_APPLICATION);
    wincl.hCursor = LoadCursor (NULL, IDC_ARROW);
    wincl.lpszMenuName = NULL;                 /* No menu */
    wincl.cbClsExtra = 0;                      /* No extra bytes after the window class */
    wincl.cbWndExtra = 0;                      /* structure or the window instance */
    /* Use Windows's default colour as the background of the window */
    wincl.hbrBackground = (HBRUSH) COLOR_BACKGROUND;

    /* Register the window class, and if it fails quit the program */
    if (!RegisterClassEx (&wincl))
        return 0;

    /* The class is registered, let's create the program*/
    GMainHWND = CreateWindowEx (
           0,                   /* Extended possibilites for variation */
           szClassName,         /* Classname */
           _T("Code::Blocks Template Windows App"),       /* Title Text */
           WS_OVERLAPPEDWINDOW, /* default window */
           CW_USEDEFAULT,       /* Windows decides the position */
           CW_USEDEFAULT,       /* where the window ends up on the screen */
           544,                 /* The programs width */
           375,                 /* and height in pixels */
           HWND_DESKTOP,        /* The window is a child-window to desktop */
           NULL,                /* No menu */
           hThisInstance,       /* Program Instance handler */
           NULL                 /* No Window Creation data */
           );

    /* Make the window visible on the screen */
    ShowWindow (GMainHWND, nCmdShow);

	INITCOMMONCONTROLSEX ctrls_param;
	ctrls_param.dwSize = sizeof ctrls_param;
	ctrls_param.dwICC  = ICC_LISTVIEW_CLASSES;
	if (InitCommonControlsEx (&ctrls_param) == TRUE)
	{
		new FInfoWnd (_T ("��ʼ��common controls�ɹ���"), GMainHWND, { 100, 550 }, { 100,100 });
	}

	bool ret = superclassListView (
		hThisInstance,
		FShellFolderInListView::SubclassProc,
		GlpfnSysListView,
		GShellFolderListViewClassName,
		GShellFolderListViewClassId);

	FShellFolderInListView *desktop, *my_computer;

	CComPtr<IShellFolder> ppDesktop;
    if (SUCCEEDED(SHGetDesktopFolder (&ppDesktop)))
    {
        desktop = new FShellFolderInListView(ppDesktop, GMainHWND, {0,0}, {500,350});

        LPITEMIDLIST DriversPIDL = NULL;
        if (SUCCEEDED(SHGetFolderLocation(NULL, CSIDL_DRIVES, NULL, 0, &DriversPIDL)))
        {
            CComPtr<IShellFolder> ppDrivers;
            if (SUCCEEDED(ppDesktop->BindToObject(DriversPIDL, NULL, IID_IShellFolder, (LPVOID *)&ppDrivers)))
            {
                my_computer = new FShellFolderInListView(ppDrivers, GMainHWND, {550, 0}, {200, 350});
            }

			::CoTaskMemFree (DriversPIDL);
        }
    }
    else {
        GLog << "Get Desktop PIDL failed.\n";
    }

    /* Run the message loop. It will run until GetMessage() returns 0 */
    while (GetMessage (&messages, NULL, 0, 0))
    {
        /* Translate virtual-key messages into character messages */
        TranslateMessage(&messages);
        /* Send message to WindowProcedure */
        DispatchMessage(&messages);
    }

    /* The program return-value is 0 - The value that PostQuitMessage() gave */
    return messages.wParam;
}


/*  This function is called by the Windows function DispatchMessage()  */

LRESULT CALLBACK WindowProcedure (HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam)
{
    return MsgProcessor(hwnd, message, wParam, lParam);
}
