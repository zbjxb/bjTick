#ifndef CMDS_H
#define CMDS_H

#include "../global/globals.h"
#include <vector>
#include <list>
#include <memory>
#include <tchar.h>

enum CmdResult
{
	AllOk,
	Canceled,
	SomeError,
};

enum CmdType
{
	CopyTo,
	Move,
	Delete,
	Rename,
	CmdObject,
	UnknownCmd
};

extern std::map<CmdType, tstr> GCmdNames;

void init_cmdnames_map ();
int GetCmdCounter (CmdType _type);
tstr CreateDefaultCmdDescription (CmdType _type);

struct FFileNode
{
	FFileNode ();

	void add (tcstr& path);
	std::vector<TCHAR> build_paths ();

	bool is_ending;
	std::map<tstr, FFileNode> NameMap;
};

struct FCmdFilePathMgr
{
	//std::vector<tstr> paths_src = {
	//	_T ("C:/joke/test"),
	//	_T ("C:/joke/test/m"), // 这一行会覆盖掉上一行
	//	_T ("C:/joke/debug"),
	//	_T ("D:/a/c/"),
	//};
	void add (tcstr& path);

	// IterType must point to tstr
	template<typename IterType>
	void add (IterType first, IterType end)
	{
		for (; first != end; ++first)
		{
			add (*first);
		}
	}

	std::vector<TCHAR> build_paths ();

	FFileNode m_node;
};

struct FCmdBase;
struct FComposeCmd;

typedef std::shared_ptr<FCmdBase>		FCmdBasePtr;
typedef std::shared_ptr<FComposeCmd>	FComposeCmdPtr;

struct FCmdBase : public std::enable_shared_from_this<FCmdBase>
{
	FCmdBase (CmdType _type, tcstr& _descriptor = _T (""));
	virtual ~FCmdBase () {}

	virtual FCmdBasePtr copy ();
	virtual CmdResult run ();

	virtual void dump (int nLevel = 0);

	void setDescription (tcstr _desc);

	CmdType m_type;
	tstr m_desc;
	FCmdFilePathMgr m_from;
	FCmdFilePathMgr m_to;

	UINT m_wFunc;
	FILEOP_FLAGS m_fFlags;
};

struct FComposeCmd : public FCmdBase
{
	FComposeCmd (tcstr& _descriptor = _T ("")) :FCmdBase (CmdObject, _descriptor) {}

	bool addCmd (const FCmdBasePtr& cmd);

	virtual FCmdBasePtr copy ();
	CmdResult run ();

	virtual void dump (int nLevel = 0);

	std::list<FCmdBasePtr> m_cmds;
};

struct FCopyToCmd : public FCmdBase
{
	FCopyToCmd (tcstr& _desc = _T ("")) :FCmdBase (CopyTo, _desc) {}
};

struct FMoveCmd : public FCmdBase
{
	FMoveCmd (tcstr& _desc = _T ("")) :FCmdBase (Move, _desc) {}
};

struct FDeleteCmd : public FCmdBase
{
	FDeleteCmd (tcstr& _desc = _T ("")) :FCmdBase (Delete, _desc) {}
};

struct FRenameCmd : public FCmdBase
{
	FRenameCmd (tcstr& _desc = _T ("")) :FCmdBase (Rename, _desc) {}
};

#endif //CMDS_H

