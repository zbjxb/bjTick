#ifndef MSG_PROCESSOR_H
#define MSG_PROCESSOR_H

#include <windows.h>

LRESULT MsgProcessor (HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam);

#endif // MSG_PROCESSOR_H

