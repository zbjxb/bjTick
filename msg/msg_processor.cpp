// *** ADDED BY HEADER FIXUP ***
#include <istream>
// *** END ***
#include "msg_processor.h"
#include <shlobj.h>
#include <shlwapi.h>
#include <iostream>
#include "msg_defs.h"
#include "../global/globals.h"

LRESULT MsgProcessor (HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam)
{
    switch (message)                  /* handle the messages */
    {
    case WM_DESTROY:
        PostQuitMessage (0);       /* send a WM_QUIT to the message queue */
        break;

    case WM_CHAR:
        {
            std::cout << "Char '" << char(wParam) << "' generated" << std::endl;
            if (wParam == 'q') {
                PostQuitMessage(0);
            }
        }
        break;

    case WM_KEYDOWN:
    {
		if (isRepeatKeydown (lParam)) {
            std::cout << std::endl;
        }
        std::cout << "VK " << wParam << " down" << std::endl;

        TCHAR keyname[32] = {0};
        GetKeyNameText(lParam, keyname, 32);
        std::cout << "Key name is: " << keyname << std::endl;

        bool is_alt_down = (KF_ALTDOWN & HIWORD(lParam)) == KF_ALTDOWN;
        std::cout << "is_alt_down: " << is_alt_down <<std::endl;

        bool is_extend = (KF_EXTENDED & HIWORD(lParam)) == KF_EXTENDED;
        std::cout <<"is_extend: "<< is_extend << std::endl;
    }
    break;

    case WM_KEYUP:
        {
            std::cout << "VK " << wParam << " up" << std::endl;
        }
        break;

    case WM_CreateMainFrame:
        {

        }
        break;

    default:                      /* for messages that we don't deal with */
        return DefWindowProc (hwnd, message, wParam, lParam);
    }

    return 0;
}
