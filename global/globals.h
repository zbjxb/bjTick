#ifndef GLOBALS_H
#define GLOBALS_H

#include <windows.h>
#include <string>
#include <iostream>
#include <sstream>
#include <map>

extern HWND GMainHWND;

extern ATOM GShellFolderListViewClassId;
extern LPTSTR GShellFolderListViewClassName;
extern WNDPROC GlpfnSysListView;

extern bool GStdOutputIsOpen;

struct GLogStruct;
extern GLogStruct GLog;

#ifdef UNICODE
	typedef std::wstring tstr;
	typedef std::wostream tostream;
	typedef std::wostringstream tostrstream;
	#define tcout std::wcout
#else
	typedef std::string tstr;
	typedef std::ostream tostream;
	typedef std::ostringstream tostrstream;
	#define tcout std::cout
#endif // UNICODE

typedef const tstr tcstr;

void openStdOutput ();
void closeStdOutput ();
void setGLogger (tostream& logger);
tostream* getGLogger ();

struct GLogStruct
{
	template<typename T>
	GLogStruct& operator<<(const T& t) {
		if (GStdOutputIsOpen)
		{
			tcout << t;
		}
		if (tostream* logger = getGLogger ())
		{
			(*logger) << t;
		}
		return (*this);
	}

	GLogStruct& operator<<(tostream& (*fun)(tostream&)) {
		if (GStdOutputIsOpen)
		{
			(*fun)(tcout);
		}
		if (tostream* logger = getGLogger ())
		{
			(*fun)(*logger);
		}
		return (*this);
	}
};

bool isRepeatKeydown (LPARAM lParam);
bool superclassListView (HINSTANCE hInst, WNDPROC lpfn_wndproc, WNDPROC& old_lpfn_wndproc, LPTSTR& strClassName, ATOM& nClassId);

#endif // GLOBALS_H
