#include "globals.h"
#include <commctrl.h>
#include <tchar.h>
#include <iostream>

HWND GMainHWND;

ATOM GShellFolderListViewClassId;
LPTSTR GShellFolderListViewClassName;
WNDPROC GlpfnSysListView;

bool GStdOutputIsOpen = false;
tostream* GLoggerStream = nullptr;
GLogStruct GLog;

void setGLogger (tostream& logger)
{
	GLoggerStream = &logger;
}

tostream* getGLogger ()
{
	return GLoggerStream;
}

void openStdOutput ()
{
	GStdOutputIsOpen = true;
}

void closeStdOutput ()
{
	GStdOutputIsOpen = false;
}

bool isRepeatKeydown (LPARAM lParam)
{
	return ((KF_REPEAT & HIWORD (lParam)) == 1);
}

bool superclassListView (
	HINSTANCE hInst,
	WNDPROC lpfn_wndproc,
	WNDPROC& old_lpfn_wndproc,
	LPTSTR& strClassName,
	ATOM& nClassId)
{
	static bool was_superclassed = false;
	static LPTSTR class_name     = (LPTSTR)_T("ShellFolderListView");
	static ATOM class_id;

	if (was_superclassed)
	{
		strClassName = class_name;
		nClassId     = class_id;
		return true;
	}

	WNDCLASS wnd_lv;
	if (GetClassInfo (NULL, WC_LISTVIEW, &wnd_lv) != 0)
	{
		wnd_lv.hInstance     = hInst;
		wnd_lv.lpszClassName = class_name;
		old_lpfn_wndproc     = wnd_lv.lpfnWndProc;
		wnd_lv.lpfnWndProc   = lpfn_wndproc;

		nClassId = RegisterClass (&wnd_lv);
		if (nClassId == 0)
		{
			// failed
			DWORD dwErrCode = GetLastError ();
			return false;
		}

		class_id         = nClassId;
		strClassName     = class_name;
		was_superclassed = true;
		return true;
	}
	else
	{
		DWORD dwErrCode = GetLastError ();
		return false;
	}
}
