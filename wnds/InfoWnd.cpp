#include "InfoWnd.h"

WNDPROC FInfoWnd::wpOrigProc = nullptr;
std::map<HWND, FInfoWnd*> FInfoWnd::_WndObjMap;

LRESULT APIENTRY FInfoWnd::SubclassProc (
	HWND hwnd,
	UINT uMsg,
	WPARAM wParam,
	LPARAM lParam)
{
	if (uMsg == WM_CHAR) {
		if (wParam == 'q') {
			SendMessage (hwnd, WM_CLOSE, NULL, NULL);
			return 0;
		}
	}

	/*else if (uMsg == WM_NCHITTEST)
	{
	UINT uHitTest = CallWindowProc (wpOrigProc, hwnd, uMsg, wParam, lParam);

	if (uHitTest == HTCLIENT)
	return HTCAPTION;
	else
	return uHitTest;
	}*/

	else if (uMsg == WM_CLOSE)
	{
		auto it = _WndObjMap.find (hwnd);
		if (it != _WndObjMap.end ())
		{
			delete it->second;
			_WndObjMap.erase (it);
		}

		//return 0;
	}

	else if (uMsg == WM_DESTROY)
	{
	}

	return CallWindowProc (wpOrigProc, hwnd, uMsg, wParam, lParam);
}
