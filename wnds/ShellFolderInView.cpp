#include "ShellFolderInView.h"
#include "../global/globals.h"

bool FShellFolderInListView::_ExploreContentInNewWindow = false;
std::map<HWND, FShellFolderInListView*> FShellFolderInListView::_WndObjMap;

FShellFolderInListView::FShellFolderInListView (const CComPtr<IShellFolder>& ppShellFolder, HWND parent, POINT pos, SIZE dim) {
	m_hView = CreateWindow (
		GShellFolderListViewClassName,
		_T ("ListView"),
		WS_CHILD | WS_CLIPSIBLINGS | WS_CAPTION | WS_SYSMENU | WS_SIZEBOX | WS_VISIBLE | LVS_ICON | LVS_SHAREIMAGELISTS,
		pos.x, pos.y, dim.cx, dim.cy,
		parent,
		NULL,
		NULL,
		NULL
	);

	// Todo: add mutex
	_WndObjMap[m_hView] = this;

#pragma region "Add 4 columns"
	int col_index;

	LVCOLUMN lvc_name;
	lvc_name.mask = LVCF_TEXT | LVCF_WIDTH;
	lvc_name.pszText = _T ("名称");
	lvc_name.cx = 275;
	col_index = ListView_InsertColumn (m_hView, 0, &lvc_name);
	if (col_index == -1)
	{
		GLog << L"Insert column failed.\n";
	}

	LVCOLUMN lvc_size;
	lvc_size.mask = LVCF_TEXT | LVCF_WIDTH;
	lvc_size.pszText = _T ("大小");
	lvc_size.cx = 80;
	col_index = ListView_InsertColumn (m_hView, 1, &lvc_size);
	if (col_index == -1)
	{
		GLog << "Insert column failed.\n";
	}

	LVCOLUMN lvc_type;
	lvc_type.mask = LVCF_TEXT | LVCF_WIDTH;
	lvc_type.pszText = _T ("类型");
	lvc_type.cx = 120;
	col_index = ListView_InsertColumn (m_hView, 2, &lvc_type);
	if (col_index == -1)
	{
		GLog << "Insert column failed.\n";
	}

	LVCOLUMN lvc_modifydate;
	lvc_modifydate.mask = LVCF_TEXT | LVCF_WIDTH;
	lvc_modifydate.pszText = _T ("修改时间");
	lvc_modifydate.cx = 120;
	col_index = ListView_InsertColumn (m_hView, 3, &lvc_modifydate);
	if (col_index == -1)
	{
		GLog << "Insert column failed.\n";
	}
#pragma endregion 

#pragma region "Get & Set system image list"
	// Draw fast locating number.
	// m_hLocateLabel = CreateWindow("STATIC",)
	HIMAGELIST hLarge;   // image list for icon view
	HIMAGELIST hSmall;   // image list for other views

	SHFILEINFO shfi;

	hLarge = reinterpret_cast<HIMAGELIST>(SHGetFileInfo (
		_T ("C:\\"),
		0,
		&shfi,
		sizeof (SHFILEINFO),
		SHGFI_SYSICONINDEX | SHGFI_LARGEICON | SHGFI_ICON));

	hSmall = reinterpret_cast<HIMAGELIST>(SHGetFileInfo (
		_T ("C:\\"),
		0,
		&shfi,
		sizeof (SHFILEINFO),
		SHGFI_SYSICONINDEX | SHGFI_SMALLICON | SHGFI_ICON));

	ListView_SetImageList (m_hView, hLarge, LVSIL_NORMAL);
	ListView_SetImageList (m_hView, hSmall, LVSIL_SMALL);
#pragma endregion 

	m_lstHistory.clear ();
	m_nHistoryLevel = -1;
	TGuardValue<bool> guard (m_bInHistory, false);
	DisplayFolderInCurrentWindow (ppShellFolder);
}

void FShellFolderInListView::DisplayFolderInCurrentWindow (const CComPtr<IShellFolder>& ppShellFolder)
{
#pragma region "Used for forward/backward exploring"
	if (!m_bInHistory)
	{
		if (m_lstHistory.size () != (m_nHistoryLevel + 1))
		{
			m_lstHistory.erase (m_lstHistory.begin () + m_nHistoryLevel + 1, m_lstHistory.end ());
		}

		m_lstHistory.push_back (ppShellFolder);
		++m_nHistoryLevel;
	}
#pragma endregion

	///< disable redraw
	SendMessage (m_hView, WM_SETREDRAW, FALSE, NULL);
	///< enable redraw
	ExitCaller (enable_redraw, ([=] {SendMessage (m_hView, WM_SETREDRAW, TRUE, NULL); }));

	///< clear view
	ListView_DeleteAllItems (m_hView);

	m_pShellFolder = ppShellFolder;
	if (m_pShellFolder)
	{
		CComPtr<IMalloc> pMalloc;
		if (SUCCEEDED (SHGetMalloc (&pMalloc)))
		{
			LVITEM lvI;
			lvI.mask = LVIF_TEXT | LVIF_IMAGE | LVIF_STATE;
			lvI.state = 0;
			lvI.stateMask = 0;

			LPITEMIDLIST pidlItems = NULL;
			CComPtr<IEnumIDList> ppenum;

			ULONG celtFetched;
			int index = 0;

			LPITEMIDLIST ROOT_PIDL = NULL;
			if (FAILED (SHGetIDListFromObject (m_pShellFolder, &ROOT_PIDL)))
			{
				return;
			}

			ExitCaller (ROOT_PIDL, ([pMalloc, &ROOT_PIDL]() { pMalloc->Free (ROOT_PIDL); }));

			CComPtr<IShellItem> ppRootItem;
			if (FAILED (SHCreateItemFromIDList (ROOT_PIDL, IID_IShellItem, (LPVOID *)&ppRootItem)))
			{
				return;
			}

#pragma region "Using Namespace Walk"
			//CComPtr<INamespaceWalk> pnsw = NULL;

			//// Create the INamespaceWalk instance
			//if (FAILED(CoCreateInstance (CLSID_NamespaceWalker,
			//	NULL,
			//	CLSCTX_INPROC,
			//	IID_INamespaceWalk,
			//	reinterpret_cast<LPVOID *>(&pnsw))))
			//{
			//	return;
			//}

			//// Walk the desktop folder and one level of subfolders    
			//if (FAILED (pnsw->Walk (m_pShellFolder, NSWF_FILESYSTEM_ONLY, 1, NULL)))
			//{
			//	return;
			//}

			//UINT cItems;
			//PIDLIST_ABSOLUTE *ppidls;
			//ExitCaller (ppidls, ([&ppidls, &cItems]() { FreeIDListArrayFull (ppidls, cItems); }));

			//// Retrieve the array of PIDLs gathered in the walk
			//if (FAILED (pnsw->GetIDArrayResult (&cItems, &ppidls)))
			//{
			//	return;
			//}

			//// Perform some action using the PIDLs

#pragma endregion 

			GLog << std::endl;
			GLog << L"Begin showing: \n";
			std::vector<std::wstring> slowItems;

			HRESULT hr = m_pShellFolder->EnumObjects (NULL, SHCONTF_FOLDERS | SHCONTF_NONFOLDERS, &ppenum);
			if (SUCCEEDED (hr) && ppenum)
			{
				while ((hr = ppenum->Next (1, &pidlItems, &celtFetched)) == S_OK && (celtFetched) == 1)
				{
					do {
						lvI.iItem = index;

						GLog << "Check attributes ... ";

						CComPtr<IShellItem> ppCurrItem;
						if (FAILED (SHCreateItemWithParent (ROOT_PIDL, NULL, pidlItems, IID_IShellItem, reinterpret_cast<LPVOID *>(&ppCurrItem))))
						{
							break;
						}

						LPTSTR pszDisplayName = nullptr;
						if (FAILED (ppCurrItem->GetDisplayName (SIGDN_PARENTRELATIVEFORADDRESSBAR, &pszDisplayName)))
						{
							break;
						}

						SFGAOF sfgaoCurrItem;
						if (FAILED (ppCurrItem->GetAttributes (SFGAO_FILESYSTEM, &sfgaoCurrItem)))
						{
							break;
						}

						///< we only display filesystem object
						if (!(sfgaoCurrItem & SFGAO_FILESYSTEM))
						{
							GLog << "Not filesystem object, skip it\n";
							break;
						}

						GLog << "Found filesystem object, go on.\n";
						GLog << "Begin get property ... " << pszDisplayName << std::endl;

						TimeSize ts;
						GLog << "Begin get icon ... ";

						int nImgIndex = 0;
						CComQIPtr<IShellIcon> ppIcon (m_pShellFolder);
						if (!ppIcon || FAILED (ppIcon->GetIconOf (pidlItems, GIL_FORSHELL, &nImgIndex))) {
							///< GetUIObjectOf
						}

						float t = ts.s ();
						if (t > 1)
						{
							slowItems.push_back (pszDisplayName);
						}
						GLog << "Ok, got icon in " << t << " seconds.\n";

						lvI.iSubItem = 0;
						lvI.pszText = pszDisplayName;
						lvI.iImage = nImgIndex;
						// message.
						if (ListView_InsertItem (m_hView, &lvI) == -1)
						{
							GLog << "Insert Item failed.\n";
							break;
						}

						CComPtr<IShellFolder2> ppShellFolder2;
						if (SUCCEEDED (m_pShellFolder.QueryInterface (&ppShellFolder2)))
						{
							HRESULT hRet;

							SHCOLUMNID shColId;
							shColId.fmtid = FMTID_Storage;

							VARIANT val;
							///< name property
							shColId.pid = PID_STG_NAME;
							//ppShellFolder2->GetDetailsEx (pidlItems, &shColId, &val);

							///< size property
							shColId.pid = PID_STG_SIZE;
							hRet = ppShellFolder2->GetDetailsEx (pidlItems, &shColId, &val);
							if (SUCCEEDED (hRet))
							{
								lvI.iSubItem = 1;
								if (val.vt == VT_UI8)
								{
									const ULONGLONG kilo = 1024;		// bytes
									const ULONGLONG mega = 1024 * kilo;	// bytes
									const ULONGLONG giga = 1024 * mega;	// bytes

									static tostrstream ss;

									if (val.ullVal < kilo)
									{
										ss << val.ullVal;
										ss << _T (" B");
									}
									else if (val.ullVal >= kilo && val.ullVal < mega)
									{
										ss << std::setprecision (3) << val.ullVal / double (kilo);
										ss << _T (" KB");
									}
									else if (val.ullVal >= mega && val.ullVal < giga)
									{
										ss << std::setprecision (3) << val.ullVal / double (mega);
										ss << _T (" MB");
									}
									else if (val.ullVal >= giga)
									{
										ss << std::setprecision (3) << val.ullVal / double (giga);
										ss << _T (" GB");
									}
									tstr sizeval = ss.str ();
									lvI.pszText = (LPTSTR)sizeval.c_str ();
									ss.str (_T (""));

									if (ListView_SetItem (m_hView, &lvI) == -1)
									{
										DWORD dwErr = GetLastError ();
										GLog << "Insert SubItem failed.\n";
									}
								}
							}

							///< type property
							shColId.pid = PID_STG_STORAGETYPE;
							hRet = ppShellFolder2->GetDetailsEx (pidlItems, &shColId, &val);
							if (SUCCEEDED (hRet))
							{
								lvI.iSubItem = 2;
								if (val.vt == VT_BSTR)
								{
									lvI.pszText = val.bstrVal;
									if (ListView_SetItem (m_hView, &lvI) == -1)
									{
										GLog << "Insert SubItem failed.\n";
									}
								}
							}

							///< date last modified property
							shColId.pid = PID_STG_WRITETIME;
							hRet = ppShellFolder2->GetDetailsEx (pidlItems, &shColId, &val);
							if (SUCCEEDED (hRet))
							{
								lvI.iSubItem = 3;
								if (val.vt == VT_DATE)
								{
									SYSTEMTIME st;
									if (VariantTimeToSystemTime (val.date, &st) == TRUE)
									{
										TCHAR filedate[64];
										_stprintf (filedate, _T ("%u/%u/%u %u:%02u"), st.wYear, st.wMonth, st.wDay, st.wHour, st.wMinute);
										lvI.pszText = filedate;

										if (ListView_SetItem (m_hView, &lvI) == -1)
										{
											GLog << "Insert SubItem failed.\n";
										}
									}
								}
							}
						}
						else
						{
							GLog << "Cannot query IShellFolder2 interface!\n";
						}

						GLog << "End get property.\n";
						index++;

					} while (false);

					pMalloc->Free (pidlItems);
				}
			}

			GLog << "\nSlow items:\n";
			for (int i = 0; i < slowItems.size (); i++)
			{
				GLog << slowItems[i] << std::endl;
			}
			GLog << "End showing. \n";
		}
	}
}

LRESULT APIENTRY FShellFolderInListView::SubclassProc (
	HWND hwnd,
	UINT uMsg,
	WPARAM wParam,
	LPARAM lParam)
{
	static std::vector<int> viewtable = {
		LV_VIEW_DETAILS,
		LV_VIEW_LIST,
		LV_VIEW_SMALLICON,
		LV_VIEW_TILE,
		LV_VIEW_ICON,
	};

	if (uMsg == WM_CHAR) {
		if (wParam == 'q') {
			SendMessage (hwnd, WM_CLOSE, NULL, NULL);
			return 0;
		}
		else if (wParam == '+')
		{
			DWORD dwViewLayout = ListView_GetView (hwnd);
			std::vector<int>::iterator iterView = std::find (viewtable.begin (), viewtable.end (), dwViewLayout);
			auto next = ++iterView;
			if (next == viewtable.end ())
			{
				next = viewtable.begin ();
			}
			ListView_SetView (hwnd, *next);
			return 0;
		}
		else if (wParam == '-')
		{
			DWORD dwViewLayout = ListView_GetView (hwnd);
			std::vector<int>::iterator iterView = std::find (viewtable.begin (), viewtable.end (), dwViewLayout);
			std::vector<int>::iterator prev;
			if (iterView == viewtable.begin ())
			{
				prev = viewtable.begin () + (viewtable.size () - 1);
			}
			else {
				prev = --iterView;
			}
			ListView_SetView (hwnd, *prev);
			return 0;
		}
	}

	else if ((uMsg == WM_MBUTTONDOWN) || (uMsg == WM_LBUTTONDBLCLK))
	{
		POINT pt{ GET_X_LPARAM (lParam), GET_Y_LPARAM (lParam) };

		LVHITTESTINFO hittestInfo;
		hittestInfo.pt = pt;
		int item_index = ListView_HitTest (hwnd, &hittestInfo);
		if (item_index != -1)
		{
			//int subitem_index = hittestInfo.iSubItem;
			FShellFolderInListView* view = FShellFolderInListView::FromHandle (hwnd);
			if (view)
			{
				bool IsFolder;
				tstr fullName;
				CComPtr<IShellFolder> ppi_folder = view->ItemShellFolder (item_index, IsFolder, fullName);
				if (uMsg == WM_LBUTTONDBLCLK)
				{
					if (IsFolder)
					{
						view->DisplayFolder (ppi_folder);
					}
					else
					{
						ShellExecute (NULL, _T ("open"), fullName.c_str (), NULL, NULL, SW_SHOW);
					}
				}
				else
				{
					if (IsFolder)
					{
						view->DisplayFolderInNewWindow (ppi_folder);
					}
					else
					{
						ShellExecute (NULL, _T ("open"), fullName.c_str (), NULL, NULL, SW_SHOW);
					}
				}
			}
		}

		//return 0;
	}

	else if (uMsg == WM_KEYDOWN)
	{
		if (!isRepeatKeydown (lParam))
		{
#pragma region "Debug Keyboard input message."
			TCHAR keyname[32] = { 0 };
			GetKeyNameText (lParam, keyname, 32);
			new FInfoWnd (tstr (keyname) + _T (" pressed."), GMainHWND, { 150, 550 }, { 100, 100 });
#pragma endregion 

			if (wParam == VK_BACK)
			{
				FShellFolderInListView* view = FShellFolderInListView::FromHandle (hwnd);
				if (view)
				{
					view->Backward ();
				}
			}

			else if (wParam == VK_RETURN)
			{
				UINT cnt = ListView_GetSelectedCount (hwnd);
				if (cnt == 1)
				{
					int iItem = ListView_GetNextItem (hwnd, -1, LVNI_SELECTED);
					if (iItem != -1)
					{
						FShellFolderInListView* view = FShellFolderInListView::FromHandle (hwnd);
						if (view)
						{
							bool IsFolder;
							tstr fullName;
							CComPtr<IShellFolder> ppi_folder = view->ItemShellFolder (iItem, IsFolder, fullName);
							if (IsFolder)
							{
								view->DisplayFolder (ppi_folder);
							}
							else
							{
								ShellExecute (NULL, _T ("open"), fullName.c_str (), NULL, NULL, SW_SHOW);
							}
						}
					}
				}
			}
		}
	}

	else if (uMsg == WM_SYSKEYDOWN)
	{
		if (wParam == VK_LEFT)
		{
			FShellFolderInListView* view = FShellFolderInListView::FromHandle (hwnd);
			if (view)
			{
				view->Backward ();
			}
		}
		else if (wParam == VK_RIGHT)
		{
			FShellFolderInListView* view = FShellFolderInListView::FromHandle (hwnd);
			if (view)
			{
				view->Forward ();
			}
		}
	}

	else if (uMsg == WM_MOVE)
	{
	}

	else if (uMsg == WM_CLOSE)
	{
		auto it = _WndObjMap.find (hwnd);
		if (it != _WndObjMap.end ())
		{
			delete it->second;
			_WndObjMap.erase (it);
		}

		//return 0;
	}
	else if (uMsg == WM_DESTROY)
	{
	}

	return CallWindowProc (GlpfnSysListView, hwnd, uMsg, wParam, lParam);
}
