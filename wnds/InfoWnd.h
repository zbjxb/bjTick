#ifndef INFOWND_H
#define INFOWND_H

#include <map>
#include <tchar.h>
#include "../global/globals.h"

class FInfoWnd
{
public:
	FInfoWnd (tcstr& info, HWND parent, POINT pos, SIZE dim)
	{
		m_hLabel = CreateWindow (
			_T ("BUTTON"),
			_T ("Info Wnd"),
			WS_CHILD | WS_CLIPSIBLINGS | WS_CAPTION | WS_SIZEBOX | WS_SYSMENU | WS_VISIBLE,
			pos.x, pos.y, dim.cx, dim.cy,
			parent,
			NULL,
			NULL,
			NULL
		);

		// Todo: add mutex
		_WndObjMap[m_hLabel] = this;

		// Subclass the edit control.
		if (!wpOrigProc) {
			wpOrigProc = (WNDPROC) SetWindowLongPtr (m_hLabel, GWLP_WNDPROC, (LONG_PTR)SubclassProc);
		}
		else {
			SetWindowLongPtr (m_hLabel, GWLP_WNDPROC, reinterpret_cast<LONG_PTR>(SubclassProc));
		}

		Print (info);
	}

	~FInfoWnd ()
	{
	}

	// Subclass procedure
	static LRESULT APIENTRY SubclassProc (
		HWND hwnd,
		UINT uMsg,
		WPARAM wParam,
		LPARAM lParam);

	void Print (tcstr& info)
	{
		m_strInfo = info;
		SetWindowText (m_hLabel, m_strInfo.c_str ());
	}

private:
	HWND m_hLabel;
	tstr m_strInfo;

	static WNDPROC wpOrigProc;
	static std::map<HWND, FInfoWnd*> _WndObjMap;
};

#endif //INFOWND_H

