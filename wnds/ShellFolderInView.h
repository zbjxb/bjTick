#ifndef SHELLFOLDERINVIEW_H
#define SHELLFOLDERINVIEW_H

#include <vector>
#include <iomanip>
#include <map>
#include <atlbase.h>

#include <windowsx.h>
#include <Ntquery.h>
#include <shlobj.h>

#include "../utils/RAII.h"
#include "InfoWnd.h"

class FShellFolderInListView
{
public:
	typedef std::vector<CAdapt<CComPtr<IShellFolder>>> ShellFolderList;

public:
	FShellFolderInListView (const CComPtr<IShellFolder>& ppShellFolder, HWND parent, POINT pos, SIZE dim);

	~FShellFolderInListView () {}

	bool ExploreContentsInNewWindow () const
	{
		return _ExploreContentInNewWindow;
	}

	void DisplayFolder (const CComPtr<IShellFolder>& ppShellFolder)
	{
		if (ExploreContentsInNewWindow ())
		{
			DisplayFolderInNewWindow (ppShellFolder);
		}
		else
		{
			TGuardValue<bool> guard (m_bInHistory, false);
			DisplayFolderInCurrentWindow (ppShellFolder);
		}
	}

	void DisplayFolderInCurrentWindow (const CComPtr<IShellFolder>& ppShellFolder);

	void DisplayFolderInNewWindow (const CComPtr<IShellFolder>& ppShellFolder)
	{
		new FShellFolderInListView (ppShellFolder, GMainHWND, { 0,0 }, { 500,350 });
	}

	void Backward () {
		TGuardValue<bool> guard (m_bInHistory, true);

		if (m_nHistoryLevel < 1) ///< History为空或者indicator已在开头
		{
			return;
		}
		else
		{
			--m_nHistoryLevel;
			DisplayFolderInCurrentWindow (m_lstHistory[m_nHistoryLevel]);
		}
	}

	void Forward ()
	{
		TGuardValue<bool> guard (m_bInHistory, true);

		if ((m_nHistoryLevel + 1) == m_lstHistory.size ())
		{
			///< History为空或者indicator已在末尾
			return;
		}
		else {
			++m_nHistoryLevel;
			DisplayFolderInCurrentWindow (m_lstHistory[m_nHistoryLevel]);
		}
	}

	CComPtr<IShellFolder> ItemShellFolder (int iItemIndex, bool& IsFolder, tstr& fullName)
	{
		if (m_pShellFolder)
		{
			TCHAR DisplayName[MAX_PATH];
			ListView_GetItemText (m_hView, iItemIndex, 0, DisplayName, MAX_PATH);

			LPITEMIDLIST ROOT_PIDL = nullptr;
			if (FAILED (SHGetIDListFromObject (m_pShellFolder, &ROOT_PIDL)))
			{
				return nullptr;
			}

			ExitCaller (ROOT_PIDL, ([&ROOT_PIDL]() { CoTaskMemFree (ROOT_PIDL); }));

			CComPtr<IShellItem> ppRootItem;
			if (FAILED (SHCreateItemFromIDList (ROOT_PIDL, IID_IShellItem, reinterpret_cast<LPVOID *>(&ppRootItem))))
			{
				return nullptr;
			}

			LPTSTR RootPath;
			if (FAILED (ppRootItem->GetDisplayName (SIGDN_FILESYSPATH, &RootPath)))
			{
				//return nullptr;
				///< The folder is special, maybe the Item is Driver.
			}

			static TCHAR ItemPath[MAX_PATH];
			if (PathCombine (ItemPath, RootPath, DisplayName) == nullptr)
			{
				return nullptr;
			}

			SFGAOF sfgaoOut;
			PIDLIST_ABSOLUTE Item_PIDL;
			if (FAILED (SHParseDisplayName (ItemPath, NULL, &Item_PIDL, SFGAO_FOLDER, &sfgaoOut)))
			{
				return nullptr;
			}

			if (sfgaoOut & SFGAO_FOLDER)
			{
				IsFolder = true;
			}
			else
			{
				IsFolder = false;
			}
			fullName = ItemPath;

			CComPtr<IShellFolder> ppShellFolder;
			if (FAILED (SHBindToObject (nullptr, Item_PIDL, nullptr, IID_IShellFolder, reinterpret_cast<LPVOID *>(&ppShellFolder))))
			{
				return nullptr;
			}

			return ppShellFolder;
		}

		return nullptr;
	}

	// Subclass procedure
	static LRESULT APIENTRY SubclassProc (
		HWND hwnd,
		UINT uMsg,
		WPARAM wParam,
		LPARAM lParam);

	static FShellFolderInListView* FromHandle (HWND hListView)
	{
		auto it = _WndObjMap.find (hListView);
		if (it != _WndObjMap.end ())
		{
			return it->second;
		}
		else
		{
			return nullptr;
		}
	}

private:
	HWND m_hLocateLabel;
	HWND m_hView;
	CComPtr<IShellFolder> m_pShellFolder;
	ShellFolderList m_lstHistory;
	int m_nHistoryLevel;
	bool m_bInHistory;

	static bool _ExploreContentInNewWindow;
	static std::map<HWND, FShellFolderInListView*> _WndObjMap;
};

#endif //SHELLFOLDERINVIEW_H
